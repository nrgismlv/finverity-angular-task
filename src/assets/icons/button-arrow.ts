import { Component } from "@angular/core";

@Component({
    selector: 'app-button-arrow-icon',
    styles: [':host { display: flex; }'],
    template: `
    <svg width="34" height="24" xmlns="http://www.w3.org/2000/svg" fill="white" stroke-width="1" stroke="#fff" fill-rule="evenodd" clip-rule="evenodd"><path d="M21.883 12l-7.527 6.235.644.765 9-7.521-9-7.479-.645.764 7.529 6.236h-21.884v1h21.883z"/></svg>
    `
})

export class ButtonArrowIconComponent {}