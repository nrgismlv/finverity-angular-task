import { Component } from "@angular/core";

@Component({
    selector: 'app-arrow-icon',
    styles: [':host { display: flex; }'],
    template: `
      <svg
            xmlns:dc="http://purl.org/dc/elements/1.1/"
            xmlns:cc="http://creativecommons.org/ns#"
            xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
            xmlns:svg="http://www.w3.org/2000/svg"
            xmlns="http://www.w3.org/2000/svg"
            xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
            xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
            width="16"
            height="16"
            viewBox="0 0 320 448"
            fill="#b3b3b3"
            id="svg2"
            version="1.1"
            inkscape:version="0.91 r13725"
            sodipodi:docname="triangle-right.svg">
            <title
                id="title3338">triangle-right</title>
            <defs
                id="defs4" />
            <g
                inkscape:label="Layer 1"
                inkscape:groupmode="layer"
                id="layer1"
                transform="translate(0,-604.36224)">
                <path
                style="fill-rule:evenodd;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                d="m 96,692.36226 144,144 -144,144 z"
                id="path3337"
                inkscape:connector-curvature="0" />
            </g>
            </svg>
    `
})

export class ArrowIconComponent {}