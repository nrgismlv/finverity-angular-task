import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ClientService } from "../shared/services/client.service";
import { Client } from "../shared/models/client";
import { AlertService } from "../shared/services/alert.service";
import { first } from "rxjs";

@Component({
    selector: 'app-client-form',
    templateUrl: './client-form.component.html',
    styleUrls: ['./client-form.component.scss']
  })
export class ClientFormComponent implements OnInit {
  public clients: Client;
  public submitted = false;
  public form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private alertService: AlertService,
    private clientService: ClientService,
    private route: ActivatedRoute,
    public router: Router,
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      client: this.fb.group({
        lastName: ['', Validators.required],
        name: ['', Validators.required],
        middleName: ['', Validators.required],
        date: [''],
        phone: ['', [Validators.required, Validators.maxLength(11)]],
        gender: [''],
        group: [''],
        coordinator: [''],
        allowMesaage: [''],
      }),
      address: this.fb.group({
        index: ['', Validators.required],
        country: ['', Validators.required],
        area: [''],
        city: ['', Validators.required],
        street: [''],
        house: [''],
      }),
      identity: this.fb.group({
        documentType: ['', Validators.required],
        series: ['', Validators.required],
        number: ['', Validators.required],
        issuedBy: [''],
        dateIssue: [''],
        file: [null],
      }),
  });
  }

  public createClient() {
    this.router.navigateByUrl('client-form/client'); 
  }

  public submit(event) {
    event.preventDefault();
    this.submitted = true;

    this.alertService.clear();

    if (this.form.invalid) {
        return;
    }

    this.clientService.create(this.form.value)
        .pipe(first())
        .subscribe({
            next: (data) => {
                this.alertService.success('Client created successfully', { keepAfterRouteChange: true });
                localStorage.setItem('currentUser', JSON.stringify(data));
                this.router.navigate(['created-user'], { relativeTo: this.route });
                this.resetForm(this.form);
            },
            error: error => {
                this.alertService.error(error);
            }
    });
  }

  public fillClientInfo(event) {
    event.preventDefault();
    this.router.navigateByUrl('client-form/address');
  }

  public fillClientAddress(event) {
    event.preventDefault();
    this.router.navigateByUrl('client-form/identity');
  }

  resetForm(form) {
    if (form != null) 
      form.reset();
  }
}