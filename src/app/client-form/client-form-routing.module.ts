import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientFormComponent } from './client-form.component';
import { CreateClientInfoComponent } from './components/client-create-info/create-client-info.component';
import { CreateClientAddressComponent } from './components/create-client-address/create-client-address.component';
import { CreateClientIdentityComponent } from './components/create-client-identity/create-client-identity.component';

const routes: Routes = [
  {
    path: '',
    component: ClientFormComponent,
    children: [
      {
        path: 'client',
        component: CreateClientInfoComponent,
      },
      {
        path: 'address',
        component: CreateClientAddressComponent,
      },
      {
        path: 'identity',
        component: CreateClientIdentityComponent,
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientFormRoutingModule {}
