import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { ToastrModule, ToastrService } from "ngx-toastr";
import { ButtonArrowIconComponent } from "src/assets/icons/button-arrow";
import { CalendarIconComponent } from "src/assets/icons/calendar";
import { ClientService } from "../shared/services/client.service";
import { SharedModule } from "../shared/shared.module";
import { ClientFormRoutingModule } from "./client-form-routing.module";
import { ClientFormComponent } from "./client-form.component";
import { CreateClientInfoComponent } from './components/client-create-info/create-client-info.component';
import { CreateClientAddressComponent } from './components/create-client-address/create-client-address.component';
import { CreateClientIdentityComponent } from "./components/create-client-identity/create-client-identity.component";


@NgModule({
  declarations: [
    ClientFormComponent,
    ButtonArrowIconComponent,
    CalendarIconComponent,
    CreateClientInfoComponent,
    CreateClientAddressComponent,
    CreateClientIdentityComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    SharedModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    ReactiveFormsModule,
    ClientFormRoutingModule
  ],
  exports: [
    ClientFormComponent
  ],
  providers: [ClientService, ToastrService],
})
export class ClientFormModule {}
