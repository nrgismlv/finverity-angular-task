import { Component, Input } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-client-identity',
  templateUrl: './create-client-identity.component.html',
  styleUrls: ['./create-client-identity.component.scss']
})
export class CreateClientIdentityComponent {
  @Input() form: FormGroup;
  @Input() submitted = false;

  constructor(
    public router: Router,
  ) { }
  
  // for easy access to form fields
  get f() { return this.form.controls; }

}
