import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-client-address',
  templateUrl: './create-client-address.component.html',
  styleUrls: ['./create-client-address.component.scss']
})
export class CreateClientAddressComponent {
  @Input() form: FormGroup;
  @Input() submitted = false;
  @Output() fillClientAddress = new EventEmitter();

  constructor(
    public router: Router,
  ) { }
  
  // for easy access to form fields
  get f() { return this.form.controls; }
}
