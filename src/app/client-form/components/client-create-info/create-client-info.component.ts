import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Client } from 'src/app/shared/models/client';

@Component({
  selector: 'app-create-client-info',
  templateUrl: './create-client-info.component.html',
  styleUrls: ['./create-client-info.component.scss']
})
export class CreateClientInfoComponent {
  @Input() form: FormGroup;
  @Input() submitted = false;
  @Output() fillClientInfo = new EventEmitter();

  client: Client;

  constructor(
    public router: Router,
  ) { }
  
  // for easy access to form fields
  get f() { return this.form.controls; }

}
