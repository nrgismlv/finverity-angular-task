import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientFormComponent } from './client-form/client-form.component';
import { AuthGuard } from './shared/helpers/auth.guard';

const routes: Routes = [
  {
    path: 'client-form',
    loadChildren: () => import('./client-form/client-form.module').then((m) => m.ClientFormModule),
  },
  {
    path: 'created-client',
    loadChildren: () => import('./created-clients/created-clients.module').then((m) => m.CreatedClientsModule),
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
