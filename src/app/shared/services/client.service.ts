import { Injectable } from '@angular/core';
import { Client } from '../models/client';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  private userSubject: BehaviorSubject<Client>;
  public user: Observable<Client>;

  constructor(
    private http: HttpClient
) {
    this.userSubject = new BehaviorSubject<Client>(JSON.parse(localStorage.getItem('user')));
    this.user = this.userSubject.asObservable();
}

  public get userValue(): Client {
    return this.userSubject.value;
  }

  create(client: Client) {
    return this.http.post(`${environment.apiUrl}/client/create`, client);
  }

  getAll() {
    return this.http.get<Client[]>(`${environment.apiUrl}/clients`);
  }
}

