export class Client {
    client: {
        allowMesaage: string | null;
        coordinator: string;
        date: string;
        gender: string;
        group: string;
        lastName: string;
        middleName: string;
        name: string;
        phone: number;
    };
    address: {
        area: string;
        city: string;
        country: string;
        house: string;
        index: string;
        street: string;
    };
    identity: {
        dateIssue: string;
        documentType: string;
        file: any;
        issuedBy: string;
        number: string;
        series: string;
    };
    token?: string;
}
  