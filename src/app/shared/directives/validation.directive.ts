import { Directive, ElementRef, OnDestroy, OnInit, Optional } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';

import { FormHelper } from '../helpers/form-helper';

@Directive({
  selector: '[appFormValidation]',
})
export class ValidationDirective implements OnInit, OnDestroy {
  readonly destroy$ = new Subject();

  constructor(private el: ElementRef, @Optional() private ngForm: FormGroupDirective) {}

  ngOnInit() {
    this.ngForm.ngSubmit.pipe(takeUntil(this.destroy$)).subscribe(() => {
      FormHelper.showValidationErrors(this.ngForm.control);
    });
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
