import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { CreatedClientsListComponent } from "./created-clients-list.component";
import { CreatedClientsRoutingModule } from "./created-clients-routing.module";

@NgModule({
    declarations: [
        CreatedClientsListComponent,
    ],
    imports: [
        RouterModule,
        CommonModule,
        SharedModule,
        CreatedClientsRoutingModule
    ],
    providers: [],
  })
  export class CreatedClientsModule {}