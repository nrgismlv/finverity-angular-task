import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CreatedClientsListComponent } from "./created-clients-list.component";

const routes: Routes = [
    {
      path: '',
      component: CreatedClientsListComponent,
    },
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class CreatedClientsRoutingModule {}