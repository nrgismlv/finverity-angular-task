import { Component, Input, OnInit } from '@angular/core';
import { first } from 'rxjs';
import { Client } from '../shared/models';
import { ClientService } from '../shared/services/client.service';

@Component({
  selector: 'app-created-clients-list',
  templateUrl: './created-clients-list.component.html',
  styleUrls: ['./created-clients-list.component.scss']
})
export class CreatedClientsListComponent implements OnInit {
  public clients: Client[] = [];

  constructor(private clientService: ClientService) { }

  ngOnInit(): void {
    this.clientService.getAll()
        .pipe(first())
        .subscribe(clients => this.clients = clients);
  }

}
