import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public isNewUser = false;

  constructor(
    public router: Router,
    private fb: FormBuilder,
  ) {}

  public form: FormGroup = this.fb.group({
    client: this.fb.group({
      lastName: ['', [Validators.required]],
      name: ['', [Validators.required]],
      middleName: ['', [Validators.required]],
      date: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      group: [null, [Validators.required]],
      coordinator: [null, [Validators.required]],
      allowMesaage: [null, [Validators.required]],
    }),
    address: this.fb.group({
      index: ['', [Validators.required]],
      country: [null, [Validators.required]],
      area: ['', [Validators.required]],
      city: [null, [Validators.required]],
      street: ['', [Validators.required]],
      house: [null, [Validators.required]],
    }),
    identity: this.fb.group({
      documentType: ['', [Validators.required]],
      series: ['', [Validators.required]],
      number: ['', [Validators.required]],
      issuedBy: ['', [Validators.required]],
      dateIssue: [null, [Validators.required]],
      file: [null, [Validators.required]],
    }),
  });

}
