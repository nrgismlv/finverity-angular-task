import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { ArrowIconComponent } from 'src/assets/icons/menu-arrow';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientFormComponent } from './client-form/client-form.component';
import { fakeBackendProvider } from './shared/helpers';
import { JwtInterceptor  } from './shared/helpers/client.interceptor';
import { ErrorInterceptor } from './shared/helpers/error.interceptor';
import { ClientService } from './shared/services/client.service';

const appRoutes: Routes = [
  {
      path: '',
      pathMatch: 'full',
      component: ClientFormComponent,
      children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: '/client-form/client',
        },
      ]
  },
];

@NgModule({
  declarations: [
    AppComponent,
    ArrowIconComponent,
  ],
  imports: [
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    { 
      provide: HTTP_INTERCEPTORS, 
      useClass: ErrorInterceptor, 
      multi: true 
    },    
    fakeBackendProvider,
    ClientService,
  ],
  exports: [RouterModule],
  bootstrap: [AppComponent],
})
export class AppModule { }
